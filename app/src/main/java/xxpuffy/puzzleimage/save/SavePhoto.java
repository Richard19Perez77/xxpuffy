package xxpuffy.puzzleimage.save;

import android.app.Activity;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import xxpuffy.puzzleimage.MainActivity;
import xxpuffy.puzzleimage.R;
import xxpuffy.puzzleimage.common.CommonVariables;
import xxpuffy.puzzleimage.data.Data;

/**
 * The save photo class allows for the user to save a high resolution version of
 * the puzzle they just solved.
 *
 * @author Rick
 */
public class SavePhoto {

    CommonVariables commonVariables = CommonVariables.getInstance();
    int currentImageToSave;

    public SavePhoto(final Context context, final int currentImage) {
        currentImageToSave = currentImage;

        final Activity activity = (Activity) context;
        activity.runOnUiThread(new Runnable() {

            private boolean mExternalStorageAvailable = false;
            private boolean mExternalStorageWriteable = false;

            @Override
            public void run() {
                // save current image to devices images folder
                String state = Environment.getExternalStorageState();
                // check if writing is an option
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    // We can read and write the media
                    mExternalStorageAvailable = mExternalStorageWriteable = true;
                } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                    // We can only read the media
                    mExternalStorageAvailable = true;
                    mExternalStorageWriteable = false;
                } else {
                    // Something else is wrong. It may be one of many other
                    // states, but
                    // all we need
                    // to know is we can neither read nor write
                    mExternalStorageAvailable = mExternalStorageWriteable = false;
                }

                if (mExternalStorageAvailable && mExternalStorageWriteable) {
                    // then write picture to phone
                    File path = Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

                    final String name = context.getString(R.string.artist) + "_"
                            + currentImageToSave + ".jpeg";

                    File file = new File(path, name);

                    // check for file in directory
                    if (file.exists()) {
                        commonVariables.showToast(context, "Photo Exists Already!");
                    } else {
                        try {
                            boolean b1 = path.mkdirs();
                            boolean b2 = path.exists();
                            // Make sure the Pictures directory exists.
                            if (b1 || b2) {

                                // get file into input stream
                                InputStream is = activity.getResources().openRawResource(
                                        Data.PICS[currentImageToSave]);

                                OutputStream os = new FileOutputStream(file);
                                byte[] data = new byte[is.available()];
                                is.read(data);
                                os.write(data);
                                is.close();
                                os.close();

                                commonVariables.showToast(context, "Image Saved!");

                                commonVariables.imagesSaved++;
                                MainActivity main = (MainActivity) activity;
                                main.updatePuzzleStats();

                                MediaScannerConnection
                                        .scanFile(
                                                context,
                                                new String[]{file.toString()},
                                                null,
                                                new MediaScannerConnection.OnScanCompletedListener() {
                                                    @Override
                                                    public void onScanCompleted(
                                                            String path, Uri uri) {
                                                        // new image is in phone
                                                        // database now for use

                                                    }
                                                });
                            } else {
                                commonVariables.showToast(context, "Could not make/access directory.");
                            }
                        } catch (IOException e) {
                            commonVariables.showToast(context, "ERROR making/accessing directory.");
                        }
                    }
                } else {
                    commonVariables.showToast(context, "Directory not available/writable.");
                }
            }
        });
    }
}
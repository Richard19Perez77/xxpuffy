package xxpuffy.puzzleimage.data;

import xxpuffy.puzzleimage.R;

/**
 * A class to hold the static data used across the application.
 *
 * @author Rick
 */
public class Data {

    public static int TRACK_01 = R.raw.urb;

    public final static int[] PICS = {R.drawable.image1, R.drawable.image2,
            R.drawable.image3, R.drawable.image4, R.drawable.image5,
            R.drawable.image6, R.drawable.image7, R.drawable.image8,
            R.drawable.image9, R.drawable.image10, R.drawable.image11,
            R.drawable.image12, R.drawable.image13, R.drawable.image14,
            R.drawable.image15, R.drawable.image16, R.drawable.image17
    };
}
package xxpuffy.puzzleimage.common;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.support.v4.app.Fragment;

import xxpuffy.puzzleimage.audio.MyMediaPlayer;
import xxpuffy.puzzleimage.audio.MySoundPool;

/**
 * A simple {@link Fragment} subclass.
 */
public class AudioFragment extends Fragment {

    public CommonVariables commonVariables = CommonVariables.getInstance();
    public NoisyAudioStreamReceiver noisyAudioStreamReceiver;
    private MyMediaPlayer myMediaPlayer;
    private MySoundPool mySoundPool;

    /**
     * Start of receiver inner class to handle headphones becoming unplugged
     */
    public class NoisyAudioStreamReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent
                    .getAction())) {
                // quiet the media player
                if (myMediaPlayer != null) {
                    if (commonVariables.playMusic) {
                        commonVariables.playMusic = false;
                        myMediaPlayer.pause();
                        commonVariables.showToast(context, "Music Off");
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        noisyAudioStreamReceiver = new NoisyAudioStreamReceiver();
        startPlayback();

    }

    public void myMediaPlayerResume() {
        myMediaPlayer.resume();
    }

    public void myMediaPlayerAbandonFocus() {
        myMediaPlayer.abandonFocus();
    }

    public void myMediaPlayerPause() {
        if (myMediaPlayer != null) {
            myMediaPlayer.pause();
        }
    }

    public void myMediaPlayerOnStop() {
        myMediaPlayer.onStop();
    }

    public void myMediaPlayerCleanUp() {
        if (myMediaPlayer != null) {
            myMediaPlayer.cleanUp();
            myMediaPlayer = null;
        }
    }

    public void mySoundPoolRelease() {
        if (mySoundPool != null) {
            mySoundPool.release();
            mySoundPool = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopPlayback();
    }

    private IntentFilter intentFilter = new IntentFilter(
            AudioManager.ACTION_AUDIO_BECOMING_NOISY);

    private void startPlayback() {
        getActivity().registerReceiver(noisyAudioStreamReceiver, intentFilter);
    }

    private void stopPlayback() {
        getActivity().unregisterReceiver(noisyAudioStreamReceiver);
    }

    public void mediaPlayerInit() {
        myMediaPlayer = new MyMediaPlayer(getContext());
        myMediaPlayer.init();
    }

    public void soundPoolInit() {
        mySoundPool = new MySoundPool(15, AudioManager.STREAM_MUSIC, 100);
        mySoundPool.init(getContext());
    }

    public MyMediaPlayer getMediaPlayer() {
        return myMediaPlayer;
    }

    public MySoundPool getSoundPool() {
        return mySoundPool;
    }
}
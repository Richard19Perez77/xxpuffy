package xxpuffy.puzzleimage.util;

import java.util.ArrayList;
import java.util.Random;

/**
 * Random helper to create random int array.
 */
public class RandomHelper {

    static Random random = new Random();

    /**
     * Start by creating a random array by creating random indexes.
     *
     * @param size
     * @return
     */
    public static int[] createRandomOrder(int size) {
        return getRandomOrder(size);
    }

    private static int[] getRandomOrder(int size) {
        int[] order = new int[size];
        boolean sorted;
        do {
            sorted = true;
            ArrayList<Integer> usedIndexes = createArrayListPopulated(size);

            for (int i = 0; i < size; i++) {
                int index = random.nextInt(usedIndexes.size());
                int value = usedIndexes.get(index);
                order[i] = value;
                usedIndexes.remove(index);
            }

            for (int i = 0; i < size; i++) {
                if (order[i] == i) {
                    sorted = false;
                    break;
                }
            }
        } while (!sorted);

        return order;
    }

    private static ArrayList<Integer> createArrayListPopulated(int size) {
        ArrayList list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(i);
        }
        return list;
    }

    public static int[] createPerfectOrder(int size) {
        int[] order = new int[size];
        for (int i = 0; i < order.length; i++) {
            order[i] = i;
        }
        return order;
    }
}
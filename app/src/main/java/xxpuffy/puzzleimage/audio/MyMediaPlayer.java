package xxpuffy.puzzleimage.audio;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import java.io.IOException;

import xxpuffy.puzzleimage.R;
import xxpuffy.puzzleimage.common.CommonVariables;
import xxpuffy.puzzleimage.data.Data;

/**
 * A class to extend Media Player and implement handling interfaces. I also
 * started implementing the ability to handle the sound changes due to incoming
 * notification sounds like phone or message alerts *
 *
 * @author Rick
 */
public class MyMediaPlayer implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, AudioManager.OnAudioFocusChangeListener,
        MediaPlayer.OnCompletionListener {

    CommonVariables cv = CommonVariables.getInstance();

    public MediaPlayer mediaPlayer;
    AudioManager am;
    int result;
    public float currentVolume = 0f;

    /**
     * Used in testing to tell if the headphones were unplugged or not
     */
    public boolean volumeSet = false;

    /**
     * The state MyMediaPlayer can be in.
     */
    public enum State {
        IDLE, INITIALIZED, PREPARED, STARTED, PREPARING, STOPPED, PAUSED, END, ERROR, PLAYBACK_COMPLETED
    }

    public State currentState;

    Context context;

    public MyMediaPlayer(Context context) {
        this.context = context;
    }

    /**
     * Initialize a new media player and audio manager instance. Set state to IDLE.
     */
    public void init() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setOnCompletionListener(this);
        }
        am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mediaPlayer.reset();
        currentState = State.IDLE;
    }

    /**
     * Begin playing music by getting the track and volume prepared and calling the asych prepare method. Set state to PREPARING.
     */
    public void start() {
        if (cv.playMusic)
            result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            if (currentState != State.IDLE && currentState != State.PREPARING)
                init();
            if (currentState != State.PREPARING) try {
                Uri path = Uri.parse(context.getString(R.string.PATH) + Data.TRACK_01);
                mediaPlayer.setDataSource(context, path);
                currentState = State.INITIALIZED;
                mediaPlayer
                        .setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setVolume(cv.volume, cv.volume);
                currentVolume = cv.volume;
                mediaPlayer.prepareAsync();
                currentState = State.PREPARING;
            } catch (IllegalArgumentException | IOException | SecurityException | IllegalStateException ignored) {
            }
        }
    }

    /**
     * Callback for when the media player is ready to play. On play flag start and seek to current position. Set state to STARTED.
     *
     * @param player
     */
    @Override
    public void onPrepared(MediaPlayer player) {
        // check for option to play music and resume last position
        if (currentState == State.PREPARING) {
            currentState = State.PREPARED;
            if (cv.playMusic) {
                if (cv.currentSoundPosition > 0) {
                    mediaPlayer.seekTo(cv.currentSoundPosition);
                }
                if (currentState != State.END && !player.isPlaying()) {
                    player.start();
                    currentState = State.STARTED;
                }
            }
        }
    }

    /**
     * On media player error set the state to ERROR. Reset the media player. Start the media player over.
     *
     * @param mediaPlayer
     * @param i
     * @param i2
     * @return
     */
    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        currentState = State.ERROR;
        mediaPlayer.reset();
        start();
        return true;
    }

    /**
     * Thi can be temporary or for a long time due to different events, handle them by starting the media player, quieting it, or pausing it.
     *
     * @param focusChange
     */
    @Override
    public void onAudioFocusChange(int focusChange) {
        // Handle audio lowering and raising for other phone sounds
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume play back
                if (mediaPlayer == null)
                    init();
                else if (!mediaPlayer.isPlaying()) {
                    start();
                } else {
                    mediaPlayer.setVolume(cv.volume, cv.volume);
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                // lost focus for an unbounded amount of time. stop and release
                pause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // lost focus for a short time, but we have to stop play back.
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    currentState = State.PAUSED;
                    cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (mediaPlayer != null) {
                    setNewVolume(0.1f);
                }
                break;
        }
    }

    /**
     * Reinitialize and restart the media player.
     */
    public void resume() {
        init();
        start();
    }

    /**
     * Will allow for the media player to release focus and play music from phone if playing.
     */
    public void abandonFocus() {
        if (am != null) {
            am.abandonAudioFocus(this);
        }
    }

    /**
     * Release focus of sound and save current sound position. Set player state to END.
     */
    public void pause() {
        abandonFocus();
        if (currentState != State.END && mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                currentState = State.PAUSED;
            }
            if (currentState == State.STARTED || currentState == State.PAUSED) {
                mediaPlayer.stop();
                currentState = State.STOPPED;
            }
            cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
            mediaPlayer.release();
            currentState = State.END;
            mediaPlayer = null;
        }
    }

    /**
     * Sets a new playing volume for the media player.
     *
     * @param setVolume
     */
    public void setNewVolume(Float setVolume) {
        if (currentState != State.END && mediaPlayer.isPlaying()) {
            mediaPlayer.setVolume(setVolume, setVolume);
            currentVolume = setVolume;
            volumeSet = true;
        }
    }

    /**
     * Release memory associated with the media player.
     */
    public void cleanUp() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            currentState = State.END;
            mediaPlayer = null;
        }
    }

    /**
     * On completion, restart the player. Set state to PLAYBACK_COMPLETED
     *
     * @param mp
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        currentState = State.PLAYBACK_COMPLETED;
        cv.currentSoundPosition = 0;
        start();
    }

    /**
     * If playing, stop the media player, record position of sound and set state to PAUSED.
     */
    public void onStop() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                currentState = State.PAUSED;
                cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
            }
        }
    }
}
package xxpuffy.puzzleimage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import xxpuffy.puzzleimage.common.CommonVariables;
import xxpuffy.puzzleimage.fragments.PuzzleFragment;
import xxpuffy.puzzleimage.fragments.PuzzleStatsFragment;
import xxpuffy.puzzleimage.save.SaveMusicService;

/**
 * The main activity of the application, sets the fragment placeholder to the container view. On resume it will start and animation for the user. Test new images by changeing value in AdjustablePuzzle Class line 99.
 */
public class MainActivity extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;

    public static final int WRITE_EXTERNAL_STORAGE_IMAGE = 1;
    public static final int WRITE_EXTERNAL_STORAGE_SAVE = 2;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updatePuzzleStats();
        }
    };

    /**
     * Start the creating an instance of the placeholder fragment. Run the app rater library.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.fragment_container);
        CommonVariables.getInstance().coordinatorLayout = coordinatorLayout;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_IMAGE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    PuzzleFragment puzzleFragment =
                            (PuzzleFragment) getSupportFragmentManager().findFragmentByTag(PuzzleFragment.TAG);
                    if (puzzleFragment != null) {
                        puzzleFragment.reSaveImage();
                    }
                } else {
                    Snackbar.make(coordinatorLayout, R.string.permission_not_granted, Snackbar.LENGTH_SHORT).show();
                }
                return;
            }
            case WRITE_EXTERNAL_STORAGE_SAVE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    PuzzleFragment puzzleFragment =
                            (PuzzleFragment) getSupportFragmentManager().findFragmentByTag(PuzzleFragment.TAG);
                    if (puzzleFragment != null) {
                        puzzleFragment.reSaveMusic();
                    }
                } else {
                    Snackbar.make(coordinatorLayout, R.string.permission_not_granted, Snackbar.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    /**
     * Start logging for Facebook user's. Begin the screen animation for starting the puzzle game.
     */
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(
                SaveMusicService.MUSIC_SAVED));
    }

    /**
     * Stop facebook tracking events.
     */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    /**
     * Create the default main menu layout.
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * The main menu option is to show the stats fragment if not already, depending on layout, this might not be an option.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // use a switch to run the handler for each item selected
        switch (item.getItemId()) {
            case R.id.menu_stats:
                switchToStatsFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Fragment switching is setting the stats fragment on top of the puzzle, this way when the user backs out of the stats the puzzle is still playable.
     */
    private void switchToStatsFragment() {
        PuzzleStatsFragment fragment = (PuzzleStatsFragment) getSupportFragmentManager().findFragmentByTag(PuzzleStatsFragment.TAG);
        if (fragment == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, PuzzleStatsFragment.newInstance(), PuzzleStatsFragment.TAG).addToBackStack("puzzle").commit();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Our listener for the puzzles being solved. A this time the stats should be
     * updated if showing and the listener has fired.
     */
    public void updatePuzzleStats() {
        PuzzleStatsFragment fragment = (PuzzleStatsFragment) getSupportFragmentManager().findFragmentByTag(PuzzleStatsFragment.TAG);
        if (fragment != null) {
            fragment.updatePuzzleStats();
        }
    }
}